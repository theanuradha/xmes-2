package com.lahiru.chrometocall.web.admin;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.labs.repackaged.com.google.common.collect.Maps;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.lahiru.chrometocall.guice.WebAppliationModule;
import com.lahiru.chrometocall.servlet.StaticContentHostServlet;
import com.lahiru.chrometocall.web.WebRequestUtil;

@SuppressWarnings("serial")
public class AdminServlet extends HttpServlet {
	private List<String> adminUsers = Lists.newArrayList("lordhiru@gmail.com");
	private static final Map<String, String> ADMIN_TASKS = Maps.newHashMap();

	static {
		ADMIN_TASKS.put("Delete old sessions", WebAppliationModule.TASK_DELETE_OLD_SESSIONS);
		ADMIN_TASKS.put("Send ping requests", WebAppliationModule.TASK_PING_ALL_PHONES);
		ADMIN_TASKS.put("Fix e-mail addresses", WebAppliationModule.TASK_FIX_EMAIL_ADDRESSES);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		UserService userService = UserServiceFactory.getUserService();
		User currentUser = userService.getCurrentUser();
		if (currentUser == null) {
			String loginUrl = WebRequestUtil.getLoginUrl(req);
			resp.sendRedirect(loginUrl);
			return;
		}

		if (!adminUsers.contains(currentUser.getEmail())) {
			resp.sendError(HttpServletResponse.SC_FORBIDDEN);
			return;
		}

		String taskId = req.getParameter("taskId");
		if (!Strings.isNullOrEmpty(taskId)) {
			try {
				Queue queue = QueueFactory.getDefaultQueue();
				TaskOptions options =
						TaskOptions.Builder.withUrl(ADMIN_TASKS.get(taskId) + "?email="+currentUser.getEmail())
						.method(Method.GET);
		    queue.add(options);

				req.setAttribute("msg",	 "Task executing. You will receive an email once complete.");
			} catch (Exception ex) {
				req.setAttribute("msg",	 "Could not start task - " + ex.getMessage());
			}
			
		}

		req.setAttribute("tasks", ADMIN_TASKS.keySet());
		req.getRequestDispatcher(StaticContentHostServlet.ADMIN_PAGE).forward(req, resp);
		
	}
}
