package com.lahiru.chrometocall.web.admin;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.common.collect.Lists;

@SuppressWarnings("serial")
public class CleanupOldSessions extends HttpServlet {
	private Logger log = Logger.getLogger(CleanupOldSessions.class.getName());
	@SuppressWarnings("deprecation")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			log.info("Staring session cleanup.");
			Calendar yesterday = GregorianCalendar.getInstance();
			yesterday.add(Calendar.DATE, -1);
			log.info("Removing sessions created before " + yesterday.getTime().toLocaleString());

			com.google.appengine.api.datastore.DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
			Query q = new Query("ApplicationUser");

			PreparedQuery pq = datastore.prepare(q);

			for (Entity result : pq.asIterable()) {
				String email = (String) result.getProperty("email");
				log.info("Currently processing: " + email);
				List<Key> staying = Lists.newArrayList();
				List<Key> removing = Lists.newArrayList();
				Query sessionQuery = new Query("ChannelSession")
					.setAncestor(result.getKey());
				Iterable<Entity> userSessions = datastore.prepare(sessionQuery).asIterable();

				Calendar latestSessionDate = null;
				
				for (Entity sessionEntity : userSessions) {
					Date date = (Date) sessionEntity.getProperty("creationDate");
					Calendar createdDate = GregorianCalendar.getInstance();
					createdDate.setTime(date);

					if (latestSessionDate == null || createdDate.after(latestSessionDate)) {
						latestSessionDate = createdDate;
					}

					if (createdDate.before(yesterday)) {
						removing.add(sessionEntity.getKey());
						log.info("Removing session " + sessionEntity.getKey() + ": for "
								+ email + " created at " + date.toLocaleString());
					} else {
						staying.add(sessionEntity.getKey());
						log.info("Keeping session " + sessionEntity.getKey() + ": for "
								+ email + " created at " + date.toLocaleString());
					}
				}
				
				Transaction transaction = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));
				try {
					result.setProperty("userSessions", staying);
					datastore.put(transaction, result);
					datastore.delete(transaction, removing);
					log.info("Finished procesing sessions for " + email + " Kept "
							+ staying.size() + ", Removed " + removing.size());

					// Update the user stats table.
					Query userStatQuery = new Query("UserStatistics")
						.addFilter("email", FilterOperator.EQUAL, email);
					PreparedQuery userStatsPq = datastore.prepare(userStatQuery);
					List<Entity> userStat = userStatsPq.asList(FetchOptions.Builder.withLimit(1));
					
					Entity entity;
					if (userStat.size() == 0) {
						entity = new Entity("UserStatistics");
						entity.setProperty("email", email);
						entity.setProperty("total_sessions_created", removing.size() + staying.size());
					} else {
						entity = userStat.get(0);
						long sessionSoFar = (Long) entity.getProperty("total_sessions_created");
						entity.setProperty("total_sessions_created",
								(int)sessionSoFar + staying.size() + removing.size());
					}
					entity.setProperty("last_seen_on_extension",
							latestSessionDate == null? new Date(0):latestSessionDate.getTime());
					entity.setProperty("active_sessions", staying.size());
					datastore.put(entity);

					transaction.commit();
				} catch (Exception e) {
					transaction.rollback();
					log.severe("Session cleanup failed for user: " + email +":" + e.getMessage());
				}
			}

			resp.setStatus(HttpServletResponse.SC_OK);
			resp.getWriter().write("OK");
		} catch (Exception e) {
			log.severe(e.getMessage());
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
}
