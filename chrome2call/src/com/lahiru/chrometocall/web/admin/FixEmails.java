package com.lahiru.chrometocall.web.admin;

import java.io.IOException;
import java.util.Collection;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.service.TalkToPhoneService;

@SuppressWarnings("serial")
public class FixEmails extends HttpServlet {
	private Logger log = Logger.getLogger(FixEmails.class.getName());
	@Inject Provider<TalkToPhoneService> talkToPhoneServiceProvider;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			log.info("Fixing email adresses");
			Multimap<String, Entity> emailAddressToEntityMap = ArrayListMultimap.create();

			com.google.appengine.api.datastore.DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
			Query q = new Query("ApplicationUser");

			PreparedQuery pq = datastore.prepare(q);

			for (Entity result : pq.asIterable()) {
				String email = (String) result.getProperty("email");
				if (email != null) {
					emailAddressToEntityMap.put(email.toLowerCase(), result);
				}
			}

			for (String email : emailAddressToEntityMap.keySet()) {
				if (emailAddressToEntityMap.get(email).size() >= 2) {
					log.warning("Email: " + email + " has more than one registration.");
					Entity strong = null;

					for (Entity e : emailAddressToEntityMap.get(email)) {
						if (strong == null 
								&& e.getProperty("devices") != null 
								&& e.getProperty("devices") instanceof Collection) {
							strong = e;
							log.warning("Keeping entity : " + e.getKey().toString());
						} else {
							datastore.delete(e.getKey());
							log.warning("Deleting entity : " + e.getKey().toString());
						}
					}
				}
			}

			for (Entity result : datastore.prepare(q).asIterable()) {
				String email = (String) result.getProperty("email");
				if (email != null) {
					String lowerCaseEmail = email.toLowerCase();
					if (!email.equals(lowerCaseEmail)) {
						log.warning("Changing " + email + " to " + lowerCaseEmail);
					}

					result.setProperty("email", lowerCaseEmail);
					datastore.put(result);
				}
			}

		} catch (Exception e) {
			log.severe(e.getMessage());
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
}
