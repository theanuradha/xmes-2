package com.lahiru.chrometocall.service;

import javax.annotation.Nullable;

import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.common.e.NoUserAccountExeption;
import com.lahiru.chrometocall.common.json.RegisterDeviceMessage;
import com.lahiru.chrometocall.common.json.UserInfoMessage;

/**
 * Provides methods to to manage user accounts.
 * @author lahiru
 */
public interface UserInfoService {
	/**
	 * Returns the currently logged in user's email.
	 * @return
	 */
	String currentUserEmail();

	/**
	 * Retrieve current user and device details in a JSON ready format.
	 * @return JSON ready user details object.
	 * @throws NoUserAccountExeption if the user is not a currently registered user.
	 */
	UserInfoMessage retrieveCurrentUserDetails();

	/**
	 * Retrieve current user and device details in a JSON ready format. Additionally if this is from
	 * extension it will mark the user as an extension user.
	 * 
	 * @return JSON ready user details object.
	 * @throws NoUserAccountExeption if the user is not a currently registered user.
	 */
	UserInfoMessage retrieveCurrentUserDetails(boolean isExtensionUser);

	/**
	 * If the current user is new to the system. Create a new appliation account.
	 * @param fromLocation User's current location.
	 * @throws BaseException if registering failed.
	 */
	void registerIfNew(String fromLocation, boolean isFromExtension);

	/**
	 * Check if the user with the give email address is already registered in the system.
	 * @param deviceId 
	 * @param activatedOnly 
	 */
	boolean isUserRegisteredInTheSystem(
			String emailAddress, boolean activatedOnly, @Nullable String deviceId);

	/**
	 * Register a new device with a user.
	 * @param registerDeviceMessage
	 */
	void registerDeviceForUser(RegisterDeviceMessage registerDeviceMessage, String loc);

	/**
	 * Set the device activation status for a given device Id for the currently logged in user.
	 * @param deviceId
	 * @param isActive
	 */
	void setDeviceActivationStatus(String deviceId, boolean isActive);

	/**
	 * Check if a device marked for deletion.
	 */
	boolean isDeviceMarkedForDeletion(String deviceId);

	void setDeviceMarkedForDeletion(String deviceId);
}
