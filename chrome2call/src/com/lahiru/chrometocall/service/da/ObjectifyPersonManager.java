package com.lahiru.chrometocall.service.da;

import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.lahiru.chrometocall.entitiy.Person;

public class ObjectifyPersonManager extends AbstractObjectifyManager implements
		PersonManager {
	Logger log = Logger.getLogger(ObjectifyPersonManager.class.getName());
	@Override
	public Key<Person> savePerson(Person person) {
		Objectify ofy = ObjectifyService.begin();

		return ofy.put(person);
	}

	@Override
	public Person getPerson(String email) {
		Objectify ofy = ObjectifyService.begin();
		Person person = null;
		try {
			person = ofy.get(Person.class, fixEmail(email));
		} catch (NotFoundException e) {
			log.warning("Person not found " + e.getMessage());
		}

		return person;
	}

	@Override
	public Key<Person> createKey(String email) {
		return new Key<Person>(Person.class, fixEmail(email));
	}

	private String fixEmail(String email) {
		return email.toLowerCase();
	}

}
