package com.lahiru.chrometocall.entitiy;

import java.io.Serializable;

import javax.persistence.Id;

public class Conversation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	String threadId;
	String dpUserPhoneNumber;
	String otherUserPhoneNumber;
	String contactName;

	public String getThreadId() {
		return threadId;
	}

	public void setThreadId(String threadId) {
		this.threadId = threadId;
	}

	public String getDpUserPhoneNumber() {
		return dpUserPhoneNumber;
	}

	public void setDpUserPhoneNumber(String dpUserPhoneNumber) {
		this.dpUserPhoneNumber = dpUserPhoneNumber;
	}

	public String getOtherUserPhoneNumber() {
		return otherUserPhoneNumber;
	}

	public void setOtherUserPhoneNumber(String otherUserPhoneNumber) {
		this.otherUserPhoneNumber = otherUserPhoneNumber;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public static class Builder {
		private String threadId;
		private String dpUserPhoneNumber;
		private String otherUserPhoneNumber;
		private String contactName;

		public Builder threadId(String threadId) {
			this.threadId = threadId;
			return this;
		}

		public Builder dpUserPhoneNumber(String dpUserPhoneNumber) {
			this.dpUserPhoneNumber = dpUserPhoneNumber;
			return this;
		}

		public Builder otherUserPhoneNumber(String otherUserPhoneNumber) {
			this.otherUserPhoneNumber = otherUserPhoneNumber;
			return this;
		}

		public Builder contactName(String contactName) {
			this.contactName = contactName;
			return this;
		}

		public Conversation build() {
			Conversation conversation = new Conversation();
			conversation.threadId = threadId;
			conversation.dpUserPhoneNumber = dpUserPhoneNumber;
			conversation.otherUserPhoneNumber = otherUserPhoneNumber;
			conversation.contactName = contactName;
			return conversation;
		}
	}
}
