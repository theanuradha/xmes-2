package com.lahiru.chrometocall.entitiy;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.google.appengine.api.datastore.Key;
import com.google.common.collect.Lists;

/**
 * Represents an Application User
 * @author lahiru
 */
@SuppressWarnings("serial")
@Entity
public class ApplicationUser implements Serializable {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Key uniqueUserId;
	private String email;
	private String userId;
	private Date registeredDate;
	private String registeredLocation;
	private boolean registeredInExtension;
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private List<MobileDevice> devices;
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private List<ChannelSession> userSessions;
	

	public Key getUniqueUserId() {
		return uniqueUserId;
	}

	public void setUniqueUserId(Key uniqueUserId) {
		this.uniqueUserId = uniqueUserId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getRegisteredDate() {
		return registeredDate;
	}

	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}

	public String getRegisteredLocation() {
		return registeredLocation;
	}

	public void setRegisteredLocation(String registeredLocation) {
		this.registeredLocation = registeredLocation;
	}

	public boolean isRegisteredInExtension() {
		return registeredInExtension;
	}

	public void setRegisteredInExtension(boolean registeredInExtension) {
		this.registeredInExtension = registeredInExtension;
	}

	public List<MobileDevice> getDevices() {
		return devices;
	}

	public void setDevices(List<MobileDevice> devices) {
		this.devices = devices;
	}

	public static ApplicationUserBuilder builder() {
		return new ApplicationUserBuilder();
	}

	public List<ChannelSession> getUserSessions() {
		if (this.userSessions == null) {
			this.userSessions = Lists.newArrayList();
		}
		return userSessions;
	}

	public void setUserSessions(List<ChannelSession> userSessions) {
		this.userSessions = userSessions;
	}

	public static class ApplicationUserBuilder {
		private String userId;
		private String email;
		private Date registeredDate;
		private String registeredLocation;
		private boolean registeredInExtension = false;
		private List<MobileDevice> devices = Lists.newArrayList();
		private List<ChannelSession> userSessions = Lists.newArrayList();

		public ApplicationUserBuilder addDevice(MobileDevice ... devices) {
			this.devices.addAll(Lists.newArrayList(devices));
			return this;
		}

		public ApplicationUserBuilder setUserSessions(List<ChannelSession> userSessions) {
			this.userSessions = userSessions;
			return this;
		}

		public ApplicationUserBuilder setRegisteredInExtension(boolean registeredInExtension) {
			this.registeredInExtension = registeredInExtension;
			return this;
		}

		public ApplicationUserBuilder setUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public ApplicationUserBuilder setEmail(String email) {
			this.email = email;
			return this;
		}

		public ApplicationUserBuilder setRegisteredDate(Date registeredDate) {
			this.registeredDate = registeredDate;
			return this;
		}

		public ApplicationUserBuilder setRegisteredLocation(String registeredLocation) {
			this.registeredLocation = registeredLocation;
			return this;
		}

		public ApplicationUser build() {
			ApplicationUser user = new ApplicationUser();
			user.setUserId(userId);
			user.setEmail(email);
			user.setRegisteredDate(registeredDate);
			user.setRegisteredLocation(registeredLocation);
			user.setRegisteredInExtension(registeredInExtension);
			user.setDevices(devices);
			user.setUserSessions(userSessions);

			return user;
		}
	}
}
