package com.lahiru.chrometocall.entitiy;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.annotation.Entity;

@Entity
public class Person implements Serializable {
	private static final long serialVersionUID = 4107310213302796580L;

	public Person() {}

	public Person(String email) {
		this.email = email;
	}

	@Id String email;
	private Long contactSetVersion = 1L;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getContactSetVersion() {
		return contactSetVersion;
	}

	public void setContactSetVersion(Long contactSetVersion) {
		this.contactSetVersion = contactSetVersion;
	}
}
