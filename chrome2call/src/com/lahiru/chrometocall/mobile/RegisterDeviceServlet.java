package com.lahiru.chrometocall.mobile;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.DPConstants;
import com.lahiru.chrometocall.common.json.RegisterDeviceMessage;
import com.lahiru.chrometocall.service.UserInfoService;

/**
 * Register the device with the server.
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class RegisterDeviceServlet extends HttpServlet {
	@Inject Gson gson;
	@Inject Provider<UserInfoService> userInfoServiceProvider;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//TODO : Encrypt these values.
		String location = req.getHeader("X-AppEngine-CityLatLong");
		String q = req.getParameter("q");
		q = URLDecoder.decode(q, "utf-8");

		RegisterDeviceMessage deviceMessage = gson.fromJson(q, RegisterDeviceMessage.class);

		resp.setContentType("text/plain");
		if (!Strings.isNullOrEmpty(deviceMessage.getC2dmHandler())) {
			userInfoServiceProvider.get().registerDeviceForUser(deviceMessage, location);
			resp.getWriter().write("OK");
		} else {
			resp.getWriter().write(DPConstants.RESPONCE_GCM_NOT_REGISTERED);
		}
		
	}
}
