package com.lahiru.chrometocall.common.json;

/**
 * JSON message containing Device Information.
 * @author lahiru
 */
public class DeviceInfoMessage extends JSONMessage {

	protected DeviceInfoMessage() {
		super(MessageType.DEVICE_INFO_MESSAGE);
	}

	private String extensionDeviceId;
	private String deviceName;
	private String countryCode;
	private boolean authenticated;
	private String phoneNumber;
	private long version;

	public String getExtensionDeviceId() {
		return extensionDeviceId;
	}

	public void setExtensionDeviceId(String extensionDeviceId) {
		this.extensionDeviceId = extensionDeviceId;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public static DeviceInfoMessageBuilder builder() {
		return new DeviceInfoMessageBuilder();
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public static class DeviceInfoMessageBuilder {
		private String extensionDeviceId;
		private String deviceName;
		private String countryCode;
		private String phoneNumber;
		private boolean authenticated;
		private long version;

		public DeviceInfoMessageBuilder setExtensionDeviceId(String extensionDeviceId) {
			this.extensionDeviceId = extensionDeviceId;
			return this;
		}

		public DeviceInfoMessageBuilder setDeviceName(String deviceName) {
			this.deviceName = deviceName;
			return this;
		}

		public DeviceInfoMessageBuilder setVersion(long version) {
			this.version = version;
			return this;
		}

		public DeviceInfoMessageBuilder setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
			return this;
		}

		public DeviceInfoMessageBuilder setAuthenticated(boolean authenticated) {
			this.authenticated = authenticated;
			return this;
		}

		public DeviceInfoMessageBuilder setCountryCode(String countryCode) {
			this.countryCode = countryCode;
			return this;
		}

		public DeviceInfoMessage build() {
			DeviceInfoMessage deviceInfoMessage = new DeviceInfoMessage();
			deviceInfoMessage.setDeviceName(this.deviceName);
			deviceInfoMessage.setExtensionDeviceId(this.extensionDeviceId);
			deviceInfoMessage.setCountryCode(countryCode);
			deviceInfoMessage.setAuthenticated(authenticated);
			deviceInfoMessage.setPhoneNumber(phoneNumber);
			deviceInfoMessage.setVersion(version);
	
			return deviceInfoMessage;
		}
	}
}