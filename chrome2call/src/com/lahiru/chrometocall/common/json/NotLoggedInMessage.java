package com.lahiru.chrometocall.common.json;

import java.net.URL;

/**
 * The JSON message sent when the requester is not logged in.
 * @author lahiru
 */
public class NotLoggedInMessage extends JSONMessage {
	private URL loginUrl;

	public NotLoggedInMessage(URL loginUrl) {
		super(MessageType.NOT_LOGGED_IN);
		this.setLoginUrl(loginUrl);
	}

	protected NotLoggedInMessage() {
		super(MessageType.NOT_LOGGED_IN); 
	}

	public URL getLoginUrl() {
		return loginUrl;
	}

	public void setLoginUrl(URL loginUrl) {
		this.loginUrl = loginUrl;
	}

	public static NotLoggedInMessageBuilder builder() {
		return new NotLoggedInMessageBuilder();
	}

	public static class NotLoggedInMessageBuilder {
		private URL loginUrl;

		public NotLoggedInMessageBuilder setLoginUrl(URL loginUrl) {
			this.loginUrl = loginUrl;
			return this;
		}

		public NotLoggedInMessage build() {
			return new NotLoggedInMessage(this.loginUrl);
		}
	}
}
