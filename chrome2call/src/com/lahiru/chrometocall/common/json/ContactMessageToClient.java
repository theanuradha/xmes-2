package com.lahiru.chrometocall.common.json;

import java.io.Serializable;

public class ContactMessageToClient implements Serializable, Comparable<ContactMessageToClient> {
	private static final long serialVersionUID = 8270507165989554040L;

	private long id;
	private String name;
	private String type;
	private String phoneNumber;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public int compareTo(ContactMessageToClient other) {
		if (other == null) {
			return 1;
		} else {
			if (other.id > this.id) {
				return 1;
			} else if (other.id == this.id) {
				return 0;
			} else {
				return -1;
			}
		}
	}

	public static class Builder {
		private long id;
		private String name;
		private String type;
		private String phoneNumber;

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder type(String type) {
			this.type = type;
			return this;
		}

		public Builder phoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
			return this;
		}

		public ContactMessageToClient build() {
			ContactMessageToClient contactMessage = new ContactMessageToClient();
			contactMessage.id = id;
			contactMessage.name = name;
			contactMessage.type = type;
			contactMessage.phoneNumber = phoneNumber;
			return contactMessage;
		}
	}
}
