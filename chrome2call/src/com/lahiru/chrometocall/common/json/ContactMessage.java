package com.lahiru.chrometocall.common.json;

import java.io.Serializable;
import java.util.ArrayList;

public class ContactMessage implements Serializable {
	private static final long serialVersionUID = 8270507165989554040L;
	private long id;
	private String name;
	private String picture;
	private ArrayList<ContactInfoMessage> contactInfo;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public ArrayList<ContactInfoMessage> getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ArrayList<ContactInfoMessage> contactInfo) {
		this.contactInfo = contactInfo;
	}

	public static class ContactInfoMessage implements Serializable {
		private static final long serialVersionUID = 8989956134418802996L;
		private long id;
		private String type;
		private String phoneNumber;

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getPhoneNumber() {
			return phoneNumber;
		}

		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
	}
 }
