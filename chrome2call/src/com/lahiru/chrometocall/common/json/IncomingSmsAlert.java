package com.lahiru.chrometocall.common.json;

public class IncomingSmsAlert extends PhoneAlert {
	private String no;
	private String contactName;
	private String messageContents;
	private long contactId;

	public IncomingSmsAlert() {
		setType("INCOMING_SMS");
	}

	public String getNo() {
		return no;
	}

	public String getContactName() {
		return contactName;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getMessageContents() {
		return messageContents;
	}

	public void setMessageContents(String messageContents) {
		this.messageContents = messageContents;
	}

	public long getContactId() {
		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}
}