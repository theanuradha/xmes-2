package com.lahiru.chrometocall.extension;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.json.JSONMessage;
import com.lahiru.chrometocall.service.AppChannelService;

/**
 * Handles make a call request coming from an extension.
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class EstablishChannelServlet extends HttpServlet {

	private static final String PREVIOUS_CHANNEL_ID = "PREVIOUS_CHANNEL_ID";
	@Inject Provider<AppChannelService> channelServiceProvider;
	@Inject Gson json;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String country = req.getHeader("X-AppEngine-Country");
		String region = req.getHeader("X-AppEngine-Region");
		String city = req.getHeader("X-AppEngine-City");

		String location = Strings.isNullOrEmpty(city) ? "Unknown Location" : city
				+ ", " + region + " - " + country;

		String previousChannelId = req.getParameter(PREVIOUS_CHANNEL_ID);
		String tokens[] = channelServiceProvider.get()
				.createChannel(req.getRemoteAddr(), previousChannelId, location);

		resp.setContentType("application/json");
		resp.getWriter().write(json.toJson(new JSONMessage(tokens[0], tokens[1])));
	}

}
