package com.lahiru.chrometocall.extension.app;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.json.DeviceInfoMessage;
import com.lahiru.chrometocall.common.json.UserInfoMessage;
import com.lahiru.chrometocall.service.UserInfoService;

@SuppressWarnings("serial")
public class AppIndexServlet extends HttpServlet {
	@Inject private Provider<UserInfoService> userInfoServiceProvider;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		// First we need to check if the user has any active devices under their belt. Also, if the
		// user has devices that are yet to be activated, we ask them to take care of them now.
		UserInfoMessage userDetails = userInfoServiceProvider.get()
				.retrieveCurrentUserDetails(false);

		List<DeviceInfoMessage> unAuthenticated = Lists.newArrayList();

		for (DeviceInfoMessage device : userDetails.getDevices()) {
			if (!device.isAuthenticated()) {
				unAuthenticated.add(device);
			}
		}

		if (userDetails.getDevices().size() > 0 && unAuthenticated.size() == 0) {
			// GOTO MAIN PAGE
		} else {
			req.setAttribute("user", userDetails.getUserId());
			req.setAttribute("unauthenticated", unAuthenticated);
			req.getRequestDispatcher(AppConstants.DEVICES_PAGE).forward(req, resp);
		}
	}
}
