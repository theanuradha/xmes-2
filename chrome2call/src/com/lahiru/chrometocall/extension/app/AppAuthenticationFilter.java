package com.lahiru.chrometocall.extension.app;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lahiru.chrometocall.extension.ExtensionAuthenticationFilter;
import com.lahiru.chrometocall.guice.WebAppliationModule;

/**
 * Intercepts any requests from deskphone web app and redirect to login page
 * if required.
 *  
 * @author lahiruw
 */
public class AppAuthenticationFilter extends ExtensionAuthenticationFilter {
	@SuppressWarnings("unused")
	private static final Logger log = 
			Logger.getLogger(AppAuthenticationFilter.class.getName());

	@Override
	protected void sendResponse(ServletRequest req, HttpServletResponse response, String loginURL)
			throws MalformedURLException, IOException, ServletException {
		
		req.setAttribute("loginurl", loginURL);
		response.addCookie(new Cookie("target", ((HttpServletRequest)req).getRequestURI()));
		req.getRequestDispatcher(AppConstants.LOGIN_HTML).forward(req, response);
	}

	@Override
	protected String redirectUrl() {
		return WebAppliationModule.APP_LOGIN_PAGE;
	}
}
