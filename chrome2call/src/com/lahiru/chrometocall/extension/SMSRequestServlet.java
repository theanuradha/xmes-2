package com.lahiru.chrometocall.extension;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.json.OutgoingSmsRequest;
import com.lahiru.chrometocall.service.TalkToPhoneService;

/**
 * Handles make a call request coming from an extension.
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class SMSRequestServlet extends HttpServlet {

	@Inject Provider<TalkToPhoneService> talkToPhoneServiceProvider;
	@Inject Gson gson;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String q = req.getParameter("m");
		q = URLDecoder.decode(q, "utf-8");

		OutgoingSmsRequest request = gson.fromJson(q, OutgoingSmsRequest.class);

		String status = talkToPhoneServiceProvider.get().sendSms(request);

		resp.getWriter().write(status);
	}

}
