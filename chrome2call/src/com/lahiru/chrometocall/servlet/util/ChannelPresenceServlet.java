package com.lahiru.chrometocall.servlet.util;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.channel.ChannelPresence;
import com.google.appengine.api.channel.ChannelService;
import com.google.appengine.api.channel.ChannelServiceFactory;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.service.AppChannelService;

/**
 * Handles make a call request coming from an extension.
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class ChannelPresenceServlet extends HttpServlet {

	@Inject Provider<AppChannelService> channelServiceProvider;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ChannelService channelService = ChannelServiceFactory.getChannelService();
		ChannelPresence presence = channelService.parsePresence(req);
		if (!presence.isConnected()) {
			try {
				channelServiceProvider.get().removeUserSession(presence.clientId());
			} catch (Exception e) {
				throw new BaseException(e);
			}
		}
	}

}
