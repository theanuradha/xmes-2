package com.lahiru.chrometocall.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Hosts static content pages.
 * @author lahiru
 */
@SuppressWarnings("serial")
public class StaticContentHostServlet extends HttpServlet {
	public static final String SUCCESSFUL_LOGGEDIN_PAGE = 
			"http://deskphone.mobi/firststeptoawsomeoness.html";

	public static final String DESKPHONE_HOME_PAGE = "jsp/index.jsp";

	public static final String DESKPHONE_DEVICES = "jsp/devices.jsp";

	public static final String ADMIN_PAGE = "jsp/admin.jsp";

	private final Map<String, String> CONTENT_MAP;

	public StaticContentHostServlet() {
		CONTENT_MAP = new HashMap<String, String>();
		CONTENT_MAP.put(SUCCESSFUL_LOGGEDIN_PAGE, "Login complete. Use the chrome extension.");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/html");
		resp.getWriter().write(String.format("<B> %s </B>", CONTENT_MAP.get(req.getRequestURI())));
	}
}
