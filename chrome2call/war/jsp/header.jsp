<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
<head>
	<title>DeskPhone (tm) - Beta</title>
	<script type="text/javascript" src="../jquery.js"></script>
	<script type="text/javascript" src="../js/PhoneFormat.js"></script>
	<link href="../css/redmond/jquery-ui-1.9.2.custom.css" rel="stylesheet">
	<script src="../js/jquery-1.8.3.js"></script>
	<script src="../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push([ '_setAccount', 'UA-37607339-1' ]);
	_gaq.push([ '_trackPageview' ]);

	(function() {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl'
				: 'http://www')
				+ '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();
</script>
</head>
<body style="background-color: #f5f8f9;">
	<div id="mainpanel" style="width: 600px;height: 575px;box-shadow: 0 2px 55px -6px #807c79;
	background-color: white;position: fixed; top: 0%; left: 50%; margin-left: -300px; border-radius: 15px;">
	<div id="title_panel" style="width: 100%; background-image: url('../bg.png'); background-repeat:no-repeat; background-size: 100%;
	text-align: left; vertical-align: middle; border-top-left-radius: 15px; border-top-right-radius: 14px; padding-top: 17px; padding-bottom: 17px;
	">
			<img src="../img/icon_large.png" style="width: 60px; height: 60px; margin-left: 30px; vertical-align: middle;"/>
			<span 
				style="font-family: Trebuchet MS1; font-size: 300%; ; color: white; font-weight: bolder; vertical-align: middle; margin-left: 10px">
				Deskphone (beta)
			</span>
		</div>