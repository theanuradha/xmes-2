<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>  
<html>
<body>

<form method="get" action="/admin">
	<c:if test="${not empty msg}">
	<div style="background-color: yellow;">
		${msg}
	</div>
	</c:if>
	<div style="width: 300px; border-color: #000088; border: medium;">
		<span style="font-size: 1em; color: #000088">Admin Tasks</span>
		<BR/>
		<table>
			<tr>
				<td> Select Task </td>
				<td><select id="taskId" name="taskId">
						<c:forEach var="d" items="${tasks}" varStatus="s">
							<option value="${d}">${d}</option>
						</c:forEach>
					</select></td>
				<td> <input type="submit" value="Execute"/></td>
				<tr><td colspan="3"> You will receive an email after the task execution completes </td></tr>
		</table>
	</div>
</form>
<BR/><HR/><BR/>
</body>
</html>