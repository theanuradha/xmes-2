package com.lahiru.xmes.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CallRequest implements Serializable {
	public static final String TYPE_FRESH = "FRESH";
	public static final String TYPE_SCHEDULED = "SCHEDULED";
	public static final String TYPE_FINISHED = "FINISHED";
	public static final String TYPE_DELETED = "DELETED";

	private String _id;
	private String number;
	private String note;
	private long time;
	private String type;

	public CallRequest() {
		
	}

	public CallRequest(String number, String note, long time) {
		super();
		this.number = number;
		this.note = note;
		this.time = time;
		this.type = TYPE_FRESH;
	}

	public CallRequest(String number, String note, long time, String type) {
		super();
		this.number = number;
		this.note = note;
		this.time = time;
		this.type = type;
	}

	public String getNumber() {
		return number;
	}
	public String getNote() {
		return note;
	}
	public long getTime() {
		return time;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}
}
