package com.lahiru.xmes.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.provider.ContactsContract.CommonDataKinds;
import android.util.Base64;

public class DPContact {
	private long id;
	private String name;
	private List<ContactInfo> contactInfo = new ArrayList<DPContact.ContactInfo>();
	private byte[] picture;
	private boolean isDirty;

	public DPContact(long id, String name, byte[] picture) {
		this();
		this.name = name;
		this.picture = picture;
		this.id = id;
	}

	private DPContact() {
		this.isDirty = true;
	}

	public static DPContact createEmptyContact() {
		return new DPContact();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ContactInfo> getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(List<ContactInfo> contactInfo) {
		this.contactInfo = contactInfo;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		// If its already set and the new one is null then dont override.
		if (this.picture == null || picture != null) {
			this.picture = picture;
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isDirty() {
		return isDirty;
	}

	public void setDirty(boolean isDirty) {
		this.isDirty = isDirty;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		int contactInfoHashcode = 0;

		if (contactInfo != null) {
			for (ContactInfo contact : contactInfo) {
				contactInfoHashcode += contact.hashCode();
			}
		}

		result = prime * result + contactInfoHashcode ;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Arrays.hashCode(picture);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DPContact other = (DPContact) obj;
		if (contactInfo == null) {
			if (other.contactInfo != null)
				return false;
		} else if (!contactInfo.equals(other.contactInfo))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (!Arrays.equals(picture, other.picture))
			return false;
		return true;
	}

	public JSONObject toJson() throws JSONException {
		JSONObject j = new JSONObject();
		j.put("id", id);
		j.put("name", name);

		String encodedImage = "";
		if (picture != null) {
			encodedImage = Base64.encodeToString(picture, Base64.DEFAULT);
		}
		j.put("picture", encodedImage);

		JSONArray ja = new JSONArray();
		for (ContactInfo c : contactInfo) {
			ja.put(c.toJson());
		}
		j.put("contactInfo", ja);

		return j;
	}


	public static class ContactInfo {
		private ContactType type;
		private String phoneNumber;
		private int version;
		private int _id;
		
		public ContactInfo(int _id, ContactType type, String phoneNumber, int version) {
			super();
			this.type = type;
			this.phoneNumber = phoneNumber;
			this.version = version;
			this._id = _id;
		}

		public JSONObject toJson() throws JSONException {
			JSONObject jo = new JSONObject();

			jo.put("id", _id);
			jo.put("type", type.toString());
			jo.put("phoneNumber", phoneNumber);

			return jo;
		}

		public int getVersion() {
			return version;
		}

		public void setVersion(int version) {
			this.version = version;
		}

		public int get_id() {
			return _id;
		}

		public void set_id(int _id) {
			this._id = _id;
		}

		public ContactType getType() {
			return type;
		}
		
		public void setType(ContactType type) {
			this.type = type;
		}
		
		public String getPhoneNumber() {
			return phoneNumber;
		}
		
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + _id;
			result = prime * result
					+ ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			result = prime * result + version;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ContactInfo other = (ContactInfo) obj;
			if (_id != other._id)
				return false;
			if (phoneNumber == null) {
				if (other.phoneNumber != null)
					return false;
			} else if (!phoneNumber.equals(other.phoneNumber))
				return false;
			if (type != other.type)
				return false;
			if (version != other.version)
				return false;
			return true;
		}
	}

	public enum ContactType {
		CELL, HOME, WORK, OTHER;

		public static ContactType parse(int type) {
			switch (type) {
				case CommonDataKinds.Phone.TYPE_MOBILE: return CELL;
				case CommonDataKinds.Phone.TYPE_HOME: return HOME;
				case CommonDataKinds.Phone.TYPE_WORK: return WORK;
				default: return OTHER;
			}
		}
	}
}