package com.lahiru.xmes;

import com.lahiru.xmes.service.TelephonyService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PhoneUnlockedBroadcastReciever extends BroadcastReceiver {

	@Override
	public void onReceive(final Context context, Intent intent) {
		try {
			TelephonyService.processNextCall(context);
		} catch (Exception e) {
			ErrorHandler.handleNonUiError(e);
		}
	}
}
