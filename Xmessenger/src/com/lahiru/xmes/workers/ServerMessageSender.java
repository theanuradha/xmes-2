package com.lahiru.xmes.workers;

import com.lahiru.xmes.service.BackgroundWorker;

public abstract class ServerMessageSender extends Thread {
	int retryCount = 5;
	int timeout = 5;

	public abstract void run();

	public void retry() {
		this.retryCount--;
		// No need to error handler. Just write it to the error log and retry in 2 minutes.
		if (retryCount > 0) {
			BackgroundWorker.i().perform(this, timeout);
			timeout *= 2;
		}
	}
}
