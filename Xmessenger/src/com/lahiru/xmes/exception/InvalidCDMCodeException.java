package com.lahiru.xmes.exception;

public class InvalidCDMCodeException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidCDMCodeException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidCDMCodeException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

	public InvalidCDMCodeException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public InvalidCDMCodeException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}
}
