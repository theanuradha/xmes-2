package com.lahiru.xmes.service;

import android.content.Context;

import com.lahiru.xmes.data.CallRequest;
import com.lahiru.xmes.data.CallRequestDataHandler;

public class CallQueue {
	public static void push(CallRequest cr, Context c) throws Exception {
		CallRequestDataHandler.INSTANCE(c).addCallRequest(cr);
	}

	public static CallRequest peek(Context c) {
		CallRequestDataHandler dataHandler = CallRequestDataHandler.INSTANCE(c);
		CallRequest callRequest = dataHandler.readNextActiveCallRequest();
		return callRequest;
	}	

	public static void empty(Context c) {
		CallRequestDataHandler dataHandler = CallRequestDataHandler.INSTANCE(c);
		dataHandler.deleteAllActiveCallRequests();
	}

	public static boolean isQueueEmpty(Context c) {
		CallRequestDataHandler dataHandler = CallRequestDataHandler.INSTANCE(c);
		int activeCalls = dataHandler.numActiveCalls();
		return activeCalls == 0;
	}

	public static int queueSize(Context c) {
		CallRequestDataHandler dataHandler = CallRequestDataHandler.INSTANCE(c);
		int activeCalls = dataHandler.numActiveCalls();
		return activeCalls;
	}
}
