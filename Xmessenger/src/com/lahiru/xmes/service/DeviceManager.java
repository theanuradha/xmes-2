package com.lahiru.xmes.service;

import java.util.UUID;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

import com.google.android.gcm.GCMRegistrar;
import com.lahiru.xmes.ErrorHandler;
import com.lahiru.xmes.R;

public class DeviceManager {
	Context cxt;
	private TelephonyManager pm;

	public static DeviceManager forContext(Context ctx) {
		return new DeviceManager(ctx);
	}

	private DeviceManager(Context ctx) {
		this.cxt = ctx;
		pm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
	}

	public String getPhoneNumber() {
		String num = getRegisteredPhoneNumber();

		if (num == null || num.trim().length() == 0) {
			return pm.getLine1Number() != null? pm.getLine1Number() : "";
		}
		return num;
	}

	public String getRegisteredPhoneNumber() {
		return ApplicationPreferences.getPreference(cxt,
				cxt.getString(R.string.pref_registered_phonenumber));
	}

	public String getCountry() {
		return pm.getNetworkCountryIso() != null? pm.getNetworkCountryIso() : "US";
	}

	public String getDeviceName() {
		return android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;
	}

	public boolean isNew() {
		String registrationKey = getRegistrationId();
		return (registrationKey.trim().length() == 0);
	}

	public static boolean isPostHoneycomb() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	}

	public String registerIfNew() throws Exception {
		String registrationKey = getRegistrationId();

		if (registrationKey.trim().length() == 0) {
			generateAndSaveUniqueKey();
		}

		String c2dmId = null;
		// Register with the Google Cloud Messaging Service.
		GCMRegistrar.checkDevice(cxt);
		GCMRegistrar.checkManifest(cxt);
		c2dmId = GCMRegistrar.getRegistrationId(cxt);

		if (c2dmId == "") {
			GCMRegistrar.register(cxt, cxt.getString(R.string.gcm_sender_id));
			setC2dmHandler(GCMRegistrar.getRegistrationId(cxt));
		} else {
			checkUpdateStatus();
		}

		return registrationKey;
	}

	public void checkUpdateStatus() {
		try {
			int applicationVersion = getApplicationVersion();
			PackageInfo packageInfo = cxt.getPackageManager().getPackageInfo(
					cxt.getPackageName(), 0);
			int actualVersion = packageInfo.versionCode;

			if (applicationVersion < actualVersion) {
				// Re-register.
				GCMRegistrar.register(cxt, cxt.getString(R.string.gcm_sender_id));
				// Update the new version
				setApplicationVersion(actualVersion);
			}
		} catch (Exception e) {
			ErrorHandler.handleNonUiError(e);
		}
	}

	public void reRegister() {
		// First clear out the existing ID.
		ApplicationPreferences.savePreference(cxt.getString(R.string.pref_c2dm_handler)
				, "", cxt);
		GCMRegistrar.register(cxt, cxt.getString(R.string.gcm_sender_id));
	}

	private void generateAndSaveUniqueKey() {
		// Not registered. create a unique device key. First try the ANDROID_ID
		String aId = Secure.getString(cxt.getContentResolver(), Secure.ANDROID_ID);

		// Some droid devices know to return the same ID on all devices. ID# 9774d56d682e549c.
		// If thats the case then use the device id. On some devices device id could be null.
		if (cxt.getString(R.string.droid_non_unique_app_id).equals(aId)) {
			String deviceId = pm.getDeviceId();
			aId = (deviceId != null) ? deviceId : UUID.randomUUID().toString();
		}
		ApplicationPreferences.savePreference(
				cxt.getString(R.string.pref_unique_device_id), aId, cxt);
	}

	public String getRegistrationId() {
		return ApplicationPreferences.getPreference(
				cxt, cxt.getString(R.string.pref_unique_device_id));
	}

	public synchronized String getC2dmHandler() {
		return ApplicationPreferences.getPreference(
				cxt, cxt.getString(R.string.pref_c2dm_handler));
	}

	/**
	 * Saves ID to preference + Sends the new regisration Id to the server.
	 * @param regId
	 * @throws Exception 
	 */
	public synchronized void setC2dmHandler(String regId) throws Exception {
		String previous = getC2dmHandler();
		// If we have a new reg ID.
		if (!"".equals(regId) && !regId.equals(previous)) {
			ApplicationPreferences.savePreference(cxt.getString(R.string.pref_c2dm_handler)
					, regId, cxt);
			// Sending a new device / account registration request will make sure its updated.
			DeskPhoneAccountService.updatePhoneStatus(cxt);
		}
	}

	public int getApplicationVersion() {
		String appVersion = ApplicationPreferences.getPreference(
				cxt, cxt.getString(R.string.pref_stored_appversion));
		if (appVersion == null || appVersion == "") {
			return -1;
		}
		return Integer.parseInt(appVersion);
	}

	public void setApplicationVersion(int version) {
		ApplicationPreferences.savePreference(cxt.getString(R.string.pref_stored_appversion)
				, Integer.toString(version), cxt);
	}

	public boolean isSmsNotificationEnabled() {
		String preference = ApplicationPreferences.getPreference(cxt, cxt.getString(R.string.pref_smsalertsenabled));

		if (preference == null || preference.length() == 0) {
			setSmsNotificationEnabled(true);
			return true;
		} else {
			return Boolean.parseBoolean(preference);
		}
	}

	public void setSmsNotificationEnabled(boolean eneabled) {
		ApplicationPreferences.savePreference(cxt.getString(R.string.pref_smsalertsenabled)
				, Boolean.toString(eneabled), cxt);
	}

	public boolean isCallNotificationEnabled() {
		String preference = 
				ApplicationPreferences.getPreference(cxt, cxt.getString(R.string.pref_callalertsenabled));

		if (preference == null || preference.length() == 0) {
			setSmsNotificationEnabled(true);
			return true;
		} else {
			return Boolean.parseBoolean(preference);
		}
	}

	public void setCallNotificationEnabled(boolean eneabled) {
		ApplicationPreferences.savePreference(cxt.getString(R.string.pref_callalertsenabled)
				, Boolean.toString(eneabled), cxt);
	}
}
