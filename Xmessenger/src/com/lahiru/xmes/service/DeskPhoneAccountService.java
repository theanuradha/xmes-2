package com.lahiru.xmes.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Bundle;

import com.lahiru.xmes.AbstractActivity;
import com.lahiru.xmes.R;
import com.lahiru.xmes.exception.InvalidCDMCodeException;

public class DeskPhoneAccountService {
	public static Account[] getAccounts(Context ctx) {
		AccountManager accountManager = AccountManager.get(ctx);

		return accountManager.getAccountsByType("com.google");
	}

	public static boolean authenticate(Account account, AbstractActivity act) {
		try {
			String AUTH_TOKEN_TYPE = "oauth2:https://www.googleapis.com/auth/userinfo.email";
			AccountManagerFuture<Bundle> authTokenFuture = AccountManager.get(act)
					.getAuthToken(account, AUTH_TOKEN_TYPE, null, act, null, null);
	
			Bundle result = authTokenFuture.getResult();
			String authToken = result.get(AccountManager.KEY_AUTHTOKEN).toString();

			if (authToken != null) {
				return true;
			} else {
				return false;
			}
		} catch (OperationCanceledException e) {
			return false;
		} catch (AuthenticatorException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * Register account with the server. Also it registers the account with
	 * the google's C2DM service.
	 */
	public static void registerAcount(
			Context context,
			String userEmail,
			boolean authenticated,
			String phoneNumber) throws Exception {

		PackageInfo packageInfo = context.getPackageManager().getPackageInfo(
				context.getPackageName(), 0);

		// Register account in server.
		DeviceManager dm = DeviceManager.forContext(context);
		String country = dm.getCountry();
		String phone = dm.getDeviceName();
		String registrationId = dm.getRegistrationId();
		String c2dmId = dm.getC2dmHandler();
		int version = packageInfo.versionCode;

		JSONObject requestParams = new JSONObject();
		requestParams.put("email", userEmail);
		requestParams.put("c2dmHandler", c2dmId);
		requestParams.put("phoneNumber", phoneNumber);
		requestParams.put("country", country);
		requestParams.put("name", phone);
		requestParams.put("registrationId", registrationId);
		requestParams.put("authenticated", authenticated);
		requestParams.put("dpVersion", version);

		TalkToServer.registerPhone(Encryptor.encrypt(requestParams.toString()));
		storeAccount(context, userEmail, authenticated);
		ApplicationPreferences.savePreference(
				context.getString(R.string.pref_registered_phonenumber),
				phoneNumber, context);
	}

	public static void updatePhoneStatus(Context ctx) throws Exception {
			Map<String, Boolean> accounts = loadAccounts(ctx);
			try {
				for (String email : accounts.keySet()) {
				registerAcount(
						ctx,
						email,
						accounts.get(email),
						ApplicationPreferences.getPreference(ctx,
								ctx.getString(R.string.pref_registered_phonenumber)));
				}
			} catch (InvalidCDMCodeException e) {
				// Re-registration was not succesful due to an invalid GCM code provided.
				// Try to re-register with GCM server.
				DeviceManager.forContext(ctx).reRegister();
			}
	}

	public static boolean isActivated(Context c) throws Exception {
		//TODO Can't we just rely on the local status?
		Map<String, Boolean> accounts = loadAccounts(c);
		Set<String> registeredAccounts = TalkToServer.checkForRegisteredAccounts(
				true, accounts.keySet(), DeviceManager.forContext(c).getRegistrationId());

		// Update the local status's for all new accounts.
		for (String s : registeredAccounts) {
			// When terminated, server will send a special cloud message.
			storeAccount(c, s, true);
		}
		
		return (registeredAccounts != null && registeredAccounts.size() > 0);
	}

	public static boolean isActivatedLocally(Context c) throws Exception {
		Map<String, Boolean> accounts = loadAccounts(c);
		boolean activated = false;
		for (String email : accounts.keySet()) {
			if (accounts.get(email)) {
				activated = true;
			}
		}
		return activated;
	}

	public static void loadPriorRegistrations(Context c) throws Exception {

		Account[] accounts = getAccounts(c);
		Set<String> accountsList = new HashSet<String>();
		for (Account ac : accounts) {
			accountsList.add(ac.name);
		}
		Set<String> registeredAccounts = TalkToServer.checkForRegisteredAccounts(
				true, accountsList, DeviceManager.forContext(c).getRegistrationId());

		// The accounts has been registered previously for this device. Register them automatically.
		for (String acc : registeredAccounts) {
			storeAccount(c, acc, true);
		}
	}

	public static Map<String, Boolean> loadAccounts(Context ctx)
			throws Exception {
		Map<String, Boolean> accounts = new HashMap<String, Boolean>();
		String registeredPhoneList = ApplicationPreferences
				.getPreference(ctx, ctx.getString(R.string.pref_registered_accounts));

		for (String email : registeredPhoneList.split(",")) {
			if (email.trim().length() == 0) continue;
			String[] split = email.split("=");
			if (split == null || split.length != 2) continue;
			accounts.put(split[0], Boolean.parseBoolean(split[1]));
		}

		return accounts;
		
	}

	public static void storeAccount(Context context, String userEmail,
			boolean authenticated) throws Exception {
		Map<String, Boolean> accounts = loadAccounts(context);
		accounts.put(userEmail, authenticated);
		String pref = ",";
		for (String acc : accounts.keySet()) {
			pref += acc + "=" + authenticated + ",";
		}
		ApplicationPreferences.savePreference(
				context.getString(R.string.pref_registered_accounts),
				pref,
				context);
	}

	public static List<String> getActiveAccounts(Context c) throws Exception {
		List<String> acc = new ArrayList<String>();
		Map<String, Boolean> accounts = loadAccounts(c);
		for (String a : accounts.keySet()) {
			if (accounts.get(a)) {
				acc.add(a);
			}
		}

		return acc;
	}

	/**
	 * Check if the current registered accounts are also registered in the server.
	 * Remove the ones that are no longer in the server (This will cause a re-set).
	 * @return false 
	 */
	public static boolean syncAccount(Context c) {
		int RETRY_COUNT  = 5;
		Map<String, Boolean> registeredAccounts = null;
		Set<String> stillActive = null;
		while (stillActive == null && RETRY_COUNT > 0) {
			try {
				registeredAccounts = loadAccounts(c);
				stillActive = TalkToServer.checkForRegisteredAccounts(
						false, registeredAccounts.keySet(), DeviceManager.forContext(c).getRegistrationId());
			} catch (Exception e) {}
			finally {
				RETRY_COUNT--;
			}
		}

		if (stillActive == null) {
			return false;
		} else {
			for (String account : registeredAccounts.keySet()) {
				Map<String, Boolean> activeAccountsMap = new HashMap<String, Boolean>();
				if (stillActive.contains(account)) {
					activeAccountsMap.put(account, registeredAccounts.get(account));
				}

				StringBuffer actAccounts = new StringBuffer();
				for (String act : activeAccountsMap.keySet()) {
					actAccounts.append(act + "=" + activeAccountsMap.get(act) + ",");
				}
				ApplicationPreferences.savePreference(
						c.getString(R.string.pref_registered_accounts), actAccounts.toString(), c);
			}
			return true;
		}
	}

	public static String getAccountsJSON(Context ctx) throws Exception {
		return new JSONArray(loadAccounts(ctx).keySet()).toString();
	}
}
