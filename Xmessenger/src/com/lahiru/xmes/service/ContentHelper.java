package com.lahiru.xmes.service;

import java.util.Date;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.lahiru.xmes.ErrorHandler;
import com.lahiru.xmes.data.ContactsDataHandler;
import com.lahiru.xmes.data.DPContact;

public class ContentHelper {
	public static boolean isMissedCall(Context c, Date at, String number) {
		Cursor cursor;
		try {
			cursor = c.getContentResolver().query(
					Uri.parse("content://call_log/calls"),
					null,
					android.provider.CallLog.Calls.DATE + " > " + at.getTime(), 
					null,
					android.provider.CallLog.Calls.DATE);
		
			while (cursor.moveToNext()) {
				int type =
						cursor.getInt(cursor.getColumnIndex(android.provider.CallLog.Calls.TYPE));
				String cNumber =
						cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.NUMBER));
				if (type == android.provider.CallLog.Calls.MISSED_TYPE && cNumber.equals(number)) {
					cursor.close();
					return true;
				}
			}
			cursor.close();
			return false;
		} catch (Exception e) {
			ErrorHandler.handleNonUiError(e);
			return false;
		}
	}

	public static void saveSentMessageToConversations(Context c, String number, String msg) {
//		final Uri CONTENT_URI = Uri.parse("content://sms/conversations");
//		Cursor cursor = c.getContentResolver().query(CONTENT_URI, null, null, null,  null);
//		
//		while(cursor.moveToNext()) {
//			String string = cursor.getString(cursor.getColumnIndex("thread_id"));
//			System.out.println("Thread ID: " + string);
//			Uri sent = Uri.parse("content://sms/sent");
//			Cursor c2 = c.getContentResolver().query(sent, null, "thread_id = ?", new String[]{string}, null);
//			while(c2.moveToNext()) {
//				//System.out.println("Thread " + string +":" + c2.getString(c2.getColumnIndex("address")));
//			}
//		}
//		Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
//		Cursor c3 = c.getContentResolver().query(uri, null, null, null, null);
//		while(c3.moveToFirst()) {
//			String name = c3.getString(c3.getColumnIndex("display_name"));
//			System.out.println(name);
//		}

		// First check if there is a thread for this phone number.
		String threadId;
		try {
			final Uri CONTENT_URI = Uri.parse("content://sms/conversations");
			Cursor cur  = c.getContentResolver().query(
					CONTENT_URI, null, "address = ?", new String[]{number}, null);
			threadId = null;
			
			if (cur.moveToFirst()) {
				threadId = cur.getString(cur.getColumnIndex("thread_id"));
				ContentValues toUpdate = new ContentValues();
				toUpdate.put("msg_count", Integer.parseInt(cur.getString(1)) + 1);
				toUpdate.put("snippet", msg);
				c.getContentResolver().update(CONTENT_URI, toUpdate, "thread_id = ?", new String[]{threadId});
			} else {
				// Create a new converation.
				cur = c.getContentResolver().query(CONTENT_URI, null, null, null, null);
				// Find the max thread_id and the new id will be +1 to that.
				int maxId = 0;
				while (cur.moveToNext()) {
					int tid = Integer.parseInt(cur.getString(0));
					maxId = Math.max(maxId, tid);
				}
				threadId = Integer.toString(maxId + 1);

				ContentValues toUpdate = new ContentValues();
				toUpdate.put("msg_count", Integer.parseInt(cur.getString(1)) + 1);
				toUpdate.put("snippet", msg);
				c.getContentResolver().insert(CONTENT_URI, toUpdate);
			}
		} catch (Exception ex) {
			// Device does not support conversations. Ignore.
			threadId = "0";
		}

		ContentValues values = new ContentValues();
    values.put("address",  number);
    values.put("body", msg);
    values.put("thread_id", threadId);
              
    c.getContentResolver().insert(Uri.parse("content://sms/sent"), values);
	}

	public static NameValuePair getContactDisplayNameByNumber(Context c, String number) {
    String name = "?";
    String contactId = "?";

    try {
			DPContact contact = ContactsDataHandler.i(c).getByPhoneNumber(ContactsHelper.i().formatE164(number, c));
			name = contact.getName();
			contactId = Long.toString(contact.getId());
		} catch (Exception e) {
			// Ignore. Send the UNKNOWN (?) value back.
		}
    return new BasicNameValuePair(name, contactId);
	}
}
