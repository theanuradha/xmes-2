package com.lahiru.xmes.service;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

import com.lahiru.xmes.R;

/**
 * Manage Google accounts.
 * @author lahiruw
 *
 */
public class AccountService {

	private AccountService() {}

	/**
	 * Return Google accounts in the system grouped in to following categories.
	 * 
	 * 1. Accounts already registered in the server.
	 * 2. Accounts already registered in both server and phone (and authenticated).
	 * 3. New accounts.
	 */
	public static Map<String, XmesAccount> getAvailableAccounts(Context context) throws Exception {
		AccountManager accountManager = AccountManager.get(context);
		Map<String, XmesAccount> result = new HashMap<String, AccountService.XmesAccount>();

		Account[] accountsByType = accountManager.getAccountsByType("com.google");
		if (accountsByType == null || accountsByType.length == 0) {
			return null;
		}

		for (int i=0; i <accountsByType.length; i++) {
			result.put(accountsByType[i].name, new XmesAccount(
					accountsByType[i].name, ACCOUNT_STATUS.NOT_REGISTERED));
		}

		// Check with server for registered accounts.
		//Set<String> registered = TalkToServer.checkForRegisteredAccounts(accountsByType);

//		for (String email : registered) {
//			result.put(email, new XmesAccount(email, ACCOUNT_STATUS.REGISTERED_IN_SERVER_ONLY));
//		}

		// Check for accounts registered and authenticated already in the phone.
		String registeredPhoneList = ApplicationPreferences
				.getPreference(context, context.getString(R.string.pref_registered_accounts));

		for (String email : registeredPhoneList.split(",")) {
			if (email.trim().length() == 0) continue;
			result.put(email, new XmesAccount(email, ACCOUNT_STATUS.REGISTERED_IN_PHONE));
		}
		
		return result;
	}

	public static class XmesAccount {
		
		public XmesAccount(String email, ACCOUNT_STATUS status) {
			super();
			this.email = email;
			this.status = status;
		}

		public final String email;
		public final ACCOUNT_STATUS status;
	}

	public enum ACCOUNT_STATUS {
		REGISTERED_IN_SERVER_ONLY,
		REGISTERED_IN_PHONE,
		NOT_REGISTERED
	}

	/**
	 * Register account with the server. Also it registers the account with
	 * the google's C2DM service.
	 */
	public static void registerAcount(
			Context context,
			String userEmail,
			boolean authenticated,
			String phoneNumber) throws Exception {

		// Register account in server.
		DeviceManager dm = DeviceManager.forContext(context);
		String country = dm.getCountry();
		String phone = dm.getDeviceName();
		String registrationId = dm.getRegistrationId();
		String c2dmId = dm.getC2dmHandler();

		JSONObject requestParams = new JSONObject();
		requestParams.put("email", userEmail);
		requestParams.put("c2dmHandler", c2dmId);
		requestParams.put("phoneNumber", phoneNumber);
		requestParams.put("country", country);
		requestParams.put("name", phone);
		requestParams.put("registrationId", registrationId);
		requestParams.put("authenticated", authenticated);

		TalkToServer.registerPhone(Encryptor.encrypt(requestParams.toString()));
		ApplicationPreferences.savePreference(
				context.getString(R.string.pref_registered_accounts),
				ApplicationPreferences.getPreference(context,
						context.getString(R.string.pref_registered_accounts))
						+ "," + userEmail + "=" + authenticated,
				context);
		ApplicationPreferences.savePreference(
				context.getString(R.string.pref_registered_phonenumber),
				phoneNumber, context);
	}

	public static void reRegisterAllAccounts(Context ctx) throws Exception {
		// Check for accounts registered and authenticated already in the phone.
			String registeredPhoneList = ApplicationPreferences
					.getPreference(ctx, ctx.getString(R.string.pref_registered_accounts));

			for (String email : registeredPhoneList.split(",")) {
				if (email.trim().length() == 0) continue;
				String[] split = email.split("=");
				if (split == null || split.length != 2) continue;
			registerAcount(
					ctx,
					split[0],
					Boolean.parseBoolean(split[1]),
					ApplicationPreferences.getPreference(ctx,
							ctx.getString(R.string.pref_registered_accounts)));
			}
	}
}
