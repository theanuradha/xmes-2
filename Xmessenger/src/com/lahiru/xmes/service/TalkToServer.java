package com.lahiru.xmes.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import com.lahiru.xmes.exception.InvalidCDMCodeException;
import com.lahiru.xmes.workers.InternalServerErrorException;

import android.util.Log;

/**
 * Utitlity methods to communicate with the backend server.
 * @author lahiruw
 *
 */
public class TalkToServer {
	private static final String ERROR_GCM_INVALID = "ERROR_GCM_INVALID";
	private static final String RESP_OK = "OK";

	private final static String TAG = "TalkToServer";
	public static final String PROD = "https://deskphoneapp.appspot.com";
	public static final String QA = "https://deskphone-qa.appspot.com";
	public static final String DEV = "http://192.168.1.2:60000";
	public static final String SERVER_URL = DEV;
	public static final String CHECK_REGISTERED_ACCOUNTS = SERVER_URL + "/mobile/isregistered";
	public static final String REGISTER_PHONE = SERVER_URL + "/mobile/registerPhone";
	public static final String MISSED_CALL = SERVER_URL + "/mobile/missedcall";
	public static final String ON_SMS = SERVER_URL + "/mobile/onsms";
	public static final String FORCED_CLOSE_NOTIFICATION = SERVER_URL + "/mobile/phoneError";
	public static final String ACKOWLEDGE_PING = SERVER_URL + "/mobile/pingAcknowledge";
	public static final String POST_CONTACT_DETAILS = SERVER_URL + "/mobile/syncContacts";

	/**
	 * Check with the server for the google accounts that are registered.
	 */
	public static Set<String> checkForRegisteredAccounts(
			boolean authenticatedOnly, Set<String> accounts, String deviceId) throws Exception {
		Set<String> registered;
		try {
			registered = new HashSet<String>();

			String url = CHECK_REGISTERED_ACCOUNTS + "?";
			for (String acc : accounts) {
				url += "emails=" + acc + "&";
			}
			url += "authenticatedOnly="+authenticatedOnly;
			url += "&deviceId="+deviceId;

			String response = makeGetRequest(url);
			JSONArray j = new JSONArray(response);

			for (int i= 0; i < j.length(); i++) {
				registered.add(j.getString(i));
			}

			return registered;
		} catch (Exception e) {
			Log.e(TAG, "Error communicating with the server.", e);
			throw e;
		}
	}

	private static String makeGetRequest(String url) throws IOException,
			ClientProtocolException, Exception {
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = client.execute(new HttpGet(url));

		if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			throw new InternalServerErrorException("Server " + url + " returned following error: " +
					response.getStatusLine().getStatusCode());
		}

		OutputStream outstream = new  ByteArrayOutputStream();
		response.getEntity().writeTo(outstream );

		return outstream.toString();
	}

	private static String makePostRequest(String url, List<NameValuePair> body) throws IOException,
			ClientProtocolException, Exception {
		HttpClient client = new DefaultHttpClient();
		HttpPost postRequest = new HttpPost(url);
		postRequest.setEntity(new UrlEncodedFormEntity(body));

		HttpResponse response = client.execute(postRequest);

		if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			throw new InternalServerErrorException("Server " + url
					+ " returned following error: "
					+ response.getStatusLine().getStatusCode());
		}

		OutputStream outstream = new ByteArrayOutputStream();
		response.getEntity().writeTo(outstream);

		return outstream.toString();
	}

	public static void registerPhone(String json) throws Exception {
		try {
			String url = REGISTER_PHONE + "?q=";
			url += encode(json);

			String resp = makeGetRequest(url);

			if (!RESP_OK.equals(resp)) {
				if (resp.equals(ERROR_GCM_INVALID)) {
					throw new InvalidCDMCodeException();
				} else {
					throw new Exception("Server error occured. User not saved.");
				}
			}
		} catch (Exception ex) {
			Log.e(TAG, "Error communicating with the server.", ex);
			throw ex;
		}
	}

	public static void missedCall(String json) throws Exception {
		try {
			String url = MISSED_CALL + "?q=";
			url += encode(json);

			String resp = makeGetRequest(url);

			handleNonOkResponse(resp);
		} catch (Exception ex) {
			Log.e(TAG, "Error communicating with the server.", ex);
			throw ex;
		}
	}

	public static void onSms(String json) throws Exception {
		try {
			String url = ON_SMS + "?q=";
			url += encode(json);

			String resp = makeGetRequest(url);

			handleNonOkResponse(resp);
		} catch (Exception ex) {
			Log.e(TAG, "Error communicating with the server.", ex);
			throw ex;
		}
	}

	public static void forceClosed(String json) throws Exception {
		try {
			String url = FORCED_CLOSE_NOTIFICATION + "?q=";
			url += encode(json);

			String resp = makeGetRequest(url);

			handleNonOkResponse(resp);
		} catch (Exception ex) {
			Log.e(TAG, "Error communicating with the server.", ex);
			throw ex;
		}
	}

	public static void acknowledgePingRequest(String json) throws Exception {
		oneWayServerMessage(json, ACKOWLEDGE_PING);
	}

	protected static void oneWayServerMessage(String json, String action)
			throws Exception {
		try {
			String url = action + "?q=";
			url += encode(json);

			String resp = makeGetRequest(url);

			handleNonOkResponse(resp);
		} catch (Exception ex) {
			Log.e(TAG, "Error communicating with the server.", ex);
			throw ex;
		}
	}

	protected static void postToServer(List<NameValuePair> data, String action)
			throws Exception {
		try {
			String url = action;

			String resp = makePostRequest(url, data);

			handleNonOkResponse(resp);
		} catch (Exception ex) {
			Log.e(TAG, "Error communicating with the server.", ex);
			throw ex;
		}
	}

	protected static void handleNonOkResponse(String resp)
			throws Exception {
		if (!RESP_OK.equals(resp)) {
			if (resp.equals(ERROR_GCM_INVALID)) {
				throw new InvalidCDMCodeException();
			} else {
				throw new Exception("Server error occured. " + resp);
			}
		}
	}

	private static String encode(String json)
			throws UnsupportedEncodingException {
		return URLEncoder.encode(json, "utf-8");
	}
}
